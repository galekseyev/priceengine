﻿namespace PriceEngine.Responses
{
    public class ProductPriceResponse
    {
        public int ProductId { get; set; }
        public int Quantity { get; set; }
        public decimal Price { get; set; }
    }
}