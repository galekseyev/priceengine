﻿using Microsoft.AspNetCore.Mvc;
using PriceEngine.BusinessLogic.Abstract;
using PriceEngine.Responses;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;

namespace PriceEngine.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class ProductsPriceListController : ControllerBase
    {
        private readonly IProductService _productService;

        public ProductsPriceListController(IProductService productService)
        {
            _productService = productService;
        }

        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<ActionResult<IEnumerable<ProductPriceResponse>>> GetProductsPricesList()
        {
            var productsPriceList = new List<ProductPriceResponse>();

            var products = await _productService.GetProductsAsync();

            foreach (var product in products)
            {
                for (int quantity = 1; quantity <= 50; quantity++)
                {
                    var price = _productService.CalculateProductPrice(product, quantity);

                    productsPriceList.Add(new ProductPriceResponse
                    {
                        ProductId = product.ProductId,
                        Quantity = quantity,
                        Price =  price
                    });
                }
            }

            return Ok(productsPriceList);
        }

        [HttpGet("{id}/{quantity}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<decimal>> GetProductPrice(int id, int quantity)
        {
            var product = await _productService.GetProductAsync(id);

            if (product == null)
                return NotFound();

            return Ok(_productService.CalculateProductPrice(product, quantity));
        }
    }
}
