﻿using PriceEngine.BusinessLogic.Abstract;
using PriceEngine.DataLayer.Abstract;
using PriceEngine.DataLayer.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;


namespace PriceEngine.BusinessLogic.Concrete
{
    public class ProductService : IProductService
    {
        private readonly IProductRepository _productRepository;
        public ProductService(IProductRepository productRepository)
        {
            _productRepository = productRepository;
        }

        public async Task<Product> GetProductAsync(int productId) => await _productRepository.GetProductAsync(productId);
        public async Task<IEnumerable<Product>> GetProductsAsync() => await _productRepository.GetProductsAsync();

        public decimal CalculateProductPrice(Product product, int quantity)
        {
            var cartonQty = quantity / product.UnitsPerCarton;
            var cartonsTotalPrice = cartonQty * product.PricePerCarton;

            var discount = cartonQty >= 3 ? (10 * cartonsTotalPrice) / 100 : 0;

            var productsWithoutCartonQty = quantity - (cartonQty * product.UnitsPerCarton);
            var productsWithoutCartonTotalPrice = productsWithoutCartonQty * product.PricePerUnit;

            var finalPrice = cartonsTotalPrice + productsWithoutCartonTotalPrice - discount;

            return Math.Round(finalPrice, 2);
        }
    }
}