﻿using System.Collections.Generic;
using System.Threading.Tasks;
using PriceEngine.DataLayer.Models;

namespace PriceEngine.BusinessLogic.Abstract
{
    public interface IProductService
    {
        Task<Product> GetProductAsync(int productId);
        Task<IEnumerable<Product>> GetProductsAsync();
        decimal CalculateProductPrice(Product product, int quantity);
    }
}