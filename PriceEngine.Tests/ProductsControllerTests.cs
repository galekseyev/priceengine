using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FluentAssertions;
using Microsoft.AspNetCore.Mvc;
using NSubstitute;
using PriceEngine.BusinessLogic.Abstract;
using PriceEngine.Controllers;
using PriceEngine.DataLayer.Models;
using PriceEngine.Responses;
using Xunit;

namespace PriceEngine.Tests
{
    public class ProductsControllerUnitTests
    {
        private readonly List<Product> _products = new List<Product>()
        {
            new Product
            {
                ProductId = 1,
                UnitsPerCarton = 20,
                PricePerCarton = 175
            },
            new Product()
            {
                ProductId = 2,
                UnitsPerCarton = 5,
                PricePerCarton = 825
            }
        };


        private readonly ProductsPriceListController _sut;
        private readonly IProductService _fakeProductService = Substitute.For<IProductService>();

        protected ProductsControllerUnitTests()
        {
            _fakeProductService
                .GetProductsAsync()
                .Returns(_products);

            _fakeProductService
                .CalculateProductPrice(Arg.Any<Product>(), Arg.Any<int>())
                .Returns(100.00m);

            _sut = new ProductsPriceListController(_fakeProductService);
        }


        public class GetProductsPricesListMethodTests : ProductsControllerUnitTests
        {
            [Fact]
            public async Task Should_Return_Products_Prices_List()
            {
                // act
                var actionResult = await _sut.GetProductsPricesList();

                // assert
                var result = actionResult.Result as OkObjectResult;

                result.Should().NotBeNull();

                var list = result.Value as IEnumerable<ProductPriceResponse>;

                list.Should().HaveCount(100);
            }
        }

        public class GetProductPriceTests : ProductsControllerUnitTests
        {
            [Fact]
            public async Task Should_Return_Product_Price()
            {
                //arrange
                var product = _products.First();

                _fakeProductService.GetProductAsync(Arg.Any<int>())
                    .Returns(product);

                // act
                var actionResult = await _sut.GetProductPrice(product.ProductId, 1);

                // assert 
                var result = actionResult.Result as OkObjectResult;

                result.Should().NotBeNull();
                result.Value.Should().Be(100m);
            }

            [Fact]
            public async Task Should_Return_Not_Found()
            {
                //arrange
                var product = _products.First();
                var productQuantity = 1;

                _fakeProductService.GetProductAsync(Arg.Any<int>())
                    .Returns((Product)null);

                // act
                var actionResult = await _sut.GetProductPrice(product.ProductId, productQuantity);

                // assert 
                var result = actionResult.Result as NotFoundResult;

                result.Should().NotBeNull();
            }
        }

    }
}
