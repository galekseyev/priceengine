using System.Threading.Tasks;
using FluentAssertions;
using NSubstitute;
using PriceEngine.BusinessLogic.Abstract;
using PriceEngine.BusinessLogic.Concrete;
using PriceEngine.DataLayer.Abstract;
using PriceEngine.DataLayer.Models;
using Xunit;

namespace PriceEngine.BusinessLogic.Tests
{
    public class ProductServiceUnitTests
    {
        private readonly Product _product = new Product()
        {
            ProductId = 1,
            UnitsPerCarton = 20,
            PricePerCarton = 175
        };

        private readonly IProductService _sut;
        private readonly IProductRepository _fakeProductRepository = Substitute.For<IProductRepository>();

        public ProductServiceUnitTests()
        {
            _sut = new ProductService(_fakeProductRepository);
        }

        public class CalculateProductPriceMethodTests : ProductServiceUnitTests
        {
            [Fact]
            public void Should_Calculate_Price_For_One_Item()
            {
                //arrange
                var quantity = 1;

                // act
                var result = _sut.CalculateProductPrice(_product, quantity);
                
                // assert
                result.Should().Be(_product.PricePerUnit);
            }

            [Fact]
            public void Should_Calculate_Price_For_Carton()
            {
                // act
                var result = _sut.CalculateProductPrice(_product, _product.UnitsPerCarton);

                // assert
                result.Should().Be(_product.PricePerCarton);
            }

            [Fact]
            public void Should_Calculate_Price_For_3_Cartons_With_Discount()
            {
                //arrange
                var cartonsCount = 3;
                var quantity = _product.UnitsPerCarton * cartonsCount;

                var discount = (10 * cartonsCount * _product.PricePerCarton) / 100;
                var total = cartonsCount * _product.PricePerCarton - discount;

                // act
                var result = _sut.CalculateProductPrice(_product, quantity);

                // assert
                result.Should().Be(total);
            }
        }

        
    }
}
