﻿using PriceEngine.DataLayer.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace PriceEngine.DataLayer.Abstract
{
    public interface IProductRepository
    {
        Task<Product> GetProductAsync(int productId);
        Task<IEnumerable<Product>> GetProductsAsync();
    }
}