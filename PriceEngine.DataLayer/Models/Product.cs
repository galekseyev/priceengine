﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PriceEngine.DataLayer.Models
{
    public class Product
    {
        public int ProductId { get; set; }
        public int UnitsPerCarton { get; set; }
        public decimal PricePerCarton { get; set; }

        public decimal PricePerUnit
        {
            get
            {
                var pricePerUnit = PricePerCarton / UnitsPerCarton;
                var finalPrice = pricePerUnit + pricePerUnit * 30 / 100;

                return Math.Round(finalPrice, 2);
            }
        }
    }
}
