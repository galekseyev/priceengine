﻿using PriceEngine.DataLayer.Abstract;
using PriceEngine.DataLayer.Models;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PriceEngine.DataLayer.Concrete
{
    public class ProductRepository : IProductRepository
    {
        public async Task<IEnumerable<Product>> GetProductsAsync()
        {
            var products = new List<Product>()
            {
                new Product
                {
                    ProductId = 1,
                    UnitsPerCarton = 20,
                    PricePerCarton = 175
                },
                new Product()
                {
                    ProductId = 2,
                    UnitsPerCarton = 5,
                    PricePerCarton = 825
                }
            };

            return await Task.FromResult<IEnumerable<Product>>(products);
        }

        public async Task<Product> GetProductAsync(int productId)
        {
            var products = await GetProductsAsync();

            return products.FirstOrDefault(p => p.ProductId == productId);
        }
    }
}
